# README #

### What is this repository for? ###

This repository contains my code for the Work Distribution assignment.

The details about the assignment/project objectives, design and implementation are in Details.md

There are sample screenshots from testing with POSTMAN in the sample_screenshots folder.

### How do I get set up? ###

1) Use a recent version of GNU/Linux, MacOS, Windows 10, or Linux on WSL.

2) Ensure you have a recent version of Node.js installed, for example:
```
PS C:\Users\rajat> node -v                                                                                              
v12.11.0
```
If not,  install it from https://nodejs.org/en/

3) Clone this repo onto your machine
```
$ git clone https://bitbucket.org/rajahuja/work-dist.git
```

4) Install 3rd-party NPM modules/dependencies:
```
$ cd src
$ npm install
```

5) Run the code
```
$ node ./app.js
Starting work-distribution application; Ctrl+C to exit.
Logging to file ./work-dist.log
```

6) You can check for log messages in ./work-dist.log
```
$ cat ./work-dist.log

info [Sun Oct 13 2019 21:15:55 GMT-0700 (Pacific Daylight Time)]  API Server listening on port 8080.
info [Sun Oct 13 2019 21:16:00 GMT-0700 (Pacific Daylight Time)]  Received GET request for all agents
error [Sun Oct 13 2019 21:16:04 GMT-0700 (Pacific Daylight Time)]  Could not parse valid task from request
...
info [Sun Oct 13 2019 21:16:12 GMT-0700 (Pacific Daylight Time)]  Exiting.

```

7) Use an HTTP client such as curl or POSTMAN to send requests to the endpoint. 
Examples:
```
$ curl -X PUT http://localhost:8080/ep1/task -H 'Content-Type: application/json' \
  -d '{
    "id": "task0",
    "skills": [ "skill1" ],
    "priority": "low",
    "agent_id": null
}'

$ curl -X GET http://localhost:8080/agents

$ curl -X DELETE http://localhost:8080/ep2/task -H 'Content-Type: application/json' \
  -d '{
    "id": "task5",
    "skills": [ "skill3" ],
    "priority": "low",
    "agent_id": "agent4"
}'
```

8) Examine the responses of the requests. Examples:
```
Response: 200 OK
Headers: Content-Type: application/json
Body:
 {
    "id": "task1",
    "skills": [
        "skill1",
        "skill2"
    ],
    "priority": "low",
    "agent_id": "agent4"
}   
```


### Whom do I talk to? ###

Questions? Please contact:

* Rajat Ahuja <rajat@ahuja.name>