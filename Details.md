# Details #

This document explains the objective of this project, as well as some design and implementation details.

### Objective ###
The objective of this code was to create a work distribution service that can assign tasks to agents, based on the required skills and priorities of tasks, the capabilities of agents, and availability of agents.

The service functionality is exposed by a set of HTTP REST API endpoints.

## Implementation ##
The solution has been implented in Node.js, using Express.js to handle exposing the API endpoints, parsing JSON and returning appropriate error codes and headers.

Agents are pre-defined in agents.json, and are loaded into memory by the application when it starts. This file can be edited as needed.

For simplicity, agent data is stored in memory, but in future can easily be replaced by a DB or key value store for persistence or efficieny.

### 3rd-Party NPM Modules used ###
* express
* node-logger (for logging to file instead of stdout)

### Code Structure ###
* app.js: The main entry point of the application. Initializes other modules, as well as defines API endpoints and their handlers.
* agent-store.js: Builds, stores (in-memory), updates and fetches agents. Can be replaced/augmented in future with a DB.
* distribution.js: Implements the core work distribution logic.
* validation.js: Documents task and agent sample structures and helps validate input task JSON
* agents.json: Pre-defined list of agents with IDs and supported skills to initialize/build agent list for application. 
* package.json: NPM package and dependency details.
* task.json: Sample task JSON.


### Agent and Task Definitions ###
- An agent is defined by a unique identifier, and a set of skills they possess.
- A task is defined by a unique identifier, a priority, a set of skills an agent needs to possess to handle that task and the agent currently assigned to that task, if any.


```javascript
//Sample task:
var task = {
        id: '',       // Unique Task ID
        skills: [],   // List of required skills
        priority: '', // 'low' or 'high'
        agent_id: ''  // Agent assigned, or null, if none yet
    };

//Sample agent:
var agent = {
    id: '',         // Unique Agent ID
    skillset: [],   // List of supported skills
    task_id: '',    // ID of task assigned, or null
    task_pty: '',   // Priority of task assigned, or null
    task_start: 0   // Task start time (ms since epoch), or null
};
```

### Agent and Task JSON Samples ###
Agent Sample:
```json
 {
        "id": "agent4",
        "skillset": [
            "skill1",
            "skill2"
        ],
        "task_id": "task1",
        "task_pty": "low",
        "task_start": 1571029054496
    }
```
Task Sample:
```json
{
    "id": "task1",
    "skills": [ "skill1", "skill2" ],
    "priority": "low",
    "agent_id": null
}
```

### Endpoints Implemented ###

The REST API Server has been implemented to expose the following endpoints:

Endpoint 1: Allows creating a new task and distributing that task to an agent; i.e. accepts a task object and will return that task, updated with the assigned agent if one was available.

```
Path: /ep1/task
Method Supported: PUT
Request Body: Task JSON
Response Body: Task JSON with agent assigned
HTTP Codes: 200, 400, 501 
```
Endpoint 2: Allows updating a task to mark it as completed.

```
Path: /ep2/task
Method Supported: DELETE
Request Body: Task JSON
HTTP Codes: 200, 400, 404, 501 

```
Endpoint 3: Returns the list of agents with their tasks, if any.

```
Path: /agents
Method Supported: GET
Request Body: Agent list JSON
HTTP Codes: 200, 400, 404, 501 

```

#### A Note About Endpoint Names, Methods and Error Codes  

I understand that the choice of endpoint names, methods and error codes below may not be ideal.
For example, we might want to implement the first two endpoints with POSTs, rather than PUT and/or DELETE.
Also, if we are keeping separate methods (PUT and DELETE), we could even combine them into just one /task endpoint. 
However, since asked in the assignment reuirements, I have created them separate.

## Future Improvements ##

Apart from the end-point naming suggestion above, here are some other possible future improvements to this solution:

1. Use a DB instead of storing agents in-memory
2. Return different error codes and/or error messages for further refining of different types of errors.
3. Implement better responses for unsupported/unimplemented methods. 
4. Add unit-test cases for automated testing (current testing was manual).
5. Replace node-logger with a better logging module
6. Write a service script so as to not have to stop service with Ctrl+C

