/*
 * Entry point for the Express-based REST API Server for  
 * the work distribution assignment.
 * 
 * (C) Copyright 2019 Rajat Ahuja <rajat@ahuja.name>.
 * 
 */

'use strict';

// 3rd-party npm modules
const express = require('express');
const node_logger = require('node-logger');

// Custom modules
const agent_store = require('./agent-store');
const dist = require('./distribution');
const validation = require('./validation');

// TODO: Ideally, all configurable constants below (e.g. port, logfile) could be read from a file
// or as command-line arguments or env variables

/*
 * File to log messages to
 * Note: Chosen as file in the current folder for simplicity. 
 * On Linux, should ideally be /var/log/... per Linux File Hierarchy Standard
 */
const logFile = './work-dist.log' 

// JSON-formatted file containing pre-defined agents
const agent_list_file = './agents.json'; 

// Port to expose the API Server on
const port = 8080;

var app;
var logger;

// Endpoints exposed by the API Server
const ep1 = '/ep1/task';
const ep2 = '/ep2/task';
const ep3 = '/agents';

// Endpoint 1 - Accepts a task and assigns it to an agent (if possible)
const ep1PutHandler = function(req, res) {

    // Parse the input request
    var task = req.body;
    if (!validation.is_valid_task(task)) {
        logger.error('Could not parse valid task from request');
        res.status(400).json({code: 400, error: 'Malformed input'});
        return;
    }

    logger.info(`Received PUT/assign request for task ${task.id}`);

    // Try to assign an agent
    var agent_id = dist.assign(task);

    if(agent_id !== null) {
        task.agent_id = agent_id;
        res.status(200).json(task);        
    } else {
        res.status(503).json({code: 503, error: 'No suitable agents available'}); 
    }
};

// Endpoint 2 - Accepts a task to be marked as completed
const ep2DeleteHandler = function(req, res) {

    // Parse the input request
    var task = req.body;
    if (!(task && task.id)) {
        logger.error('Could not parse valid task from request');
        res.status(400).json({code: 400, error: 'Malformed input'});
        return;
    }  

    logger.info(`Received DELETE/mark complete request for task ${task.id}`);

    // Try to unassign the task from agent
    var agent_id = dist.complete(task);

    if (agent_id === null) {
        res.status(500).json({code: 500, error: 'Agent could not be reset for this task'}); 
    } else if (agent_id === 0) {
        res.status(404).json({code: 404, error: 'Task not found'}); 
    } else {
        res.status(200).end();
    }

};

// Endpoint 3 - Returns list of agents with tasks assigned to them
const ep3GetHandler = function(req, res) {

    logger.info("Received GET request for all agents");

    // Fetch list of all agents from data store
    var agents = agent_store.get_all_agents();

    if (agents === null || agents.length === 0) {
        res.status(404).json({code: 404, error:"No agents found"});
    } else {
        res.status(200).json(agents);
    }
}

// Handles Ctrl+C 
process.on('SIGINT', function() {
    logger.info('Exiting.');
    process.exit();
});


// Entry point of the application
function main() {

    console.log('Starting work-distribution application; Ctrl+C to exit.');
    
    // Initialize logger
    logger = node_logger.createLogger(logFile);
    console.log(`Logging to file ${logFile}`);
    
    // Initialize agent store
    var agents = agent_store.load_and_build_agents(agent_list_file);

    if (agents === null) {
        logger.error("No agents could be built!");
        process.exit(-1);
    }
     
    // Instantiate Express.js app
    app = express();
    app.use(express.json());

    // Register routes and their handles
    app.put(ep1,ep1PutHandler);
    app.delete(ep2,ep2DeleteHandler);
    app.get(ep3,ep3GetHandler);

    // Start listening
    app.listen(port, function() {
        logger.info(`API Server listening on port ${port}.`);
    })

}

main();