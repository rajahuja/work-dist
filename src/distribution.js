/*
 * Implements core work distribution logic.
 *
 * (C) Copyright 2019 Rajat Ahuja <rajat@ahuja.name>.
 * 
 */

var agent_store = require('./agent-store');

// Check if a required skill is in agent's skillset
function skill_in_agent(agent, req_skill) {
    for(var skill of agent.skillset) {
        if (skill === req_skill) {
            return true;
        }
    }
    return false;
}

// Find all agents possessing all required skills
function find_skilled_agents(agents, req_skills) {
    var skilled_agents = [];
    for (var agent of agents) {
        var skilled = true;
        for (var skill of req_skills) {
            if (!skill_in_agent(agent, skill)) {
                skilled = false;
                break;
            }
        }
        if (skilled) {
            skilled_agents.push(agent);
        }
    }
    return skilled_agents;
}

// Find all free agents
function find_free_agents(agents) {
    var free_agents = [];
    for (var agent of agents) {
        if (agent.task_id === null) {
            free_agents.push(agent);
        }
    }
    return free_agents;
}

// Find all agents running lower-priority tasks 
function find_lower_pty_agents(agents, req_pty) {
    //TODO: For generic case with multiple priority levels, it's best to have levels as integers, 
    // and compare them using > operator.
    var lower_pty_agents = [];
    for (var agent of agents) {
        if (agent.task_pty === 'low' && req_pty === 'high') {
            lower_pty_agents.push(agent);
        }
    }
    return lower_pty_agents;
}

// Find agent with most recently assigned task 
function find_most_recently_assigned_agent(agents) {
    var most_recent = null;
    var latest_task_start = 0;
    for (var agent of agents){
        if ((agent.task_start !== null) && (agent.task_start > latest_task_start)) {
            latest_task_start = agent.task_start;
            most_recent = agent;
        } 
    }
    return most_recent;
} 

// Find agent running a particular task
function find_agent_by_task_id(task_id) {
    var agents = agent_store.get_all_agents();
    for (var agent of agents) {
        if (agent.task_id === task_id) {
            return agent.id;
        }
    }
    return null;
}


/* 
Find an assignable agent for the task per the requirements below:
- The agent must possess at least all the skills required by the task
- An agent cannot be assigned a task if they’re already working on a task of equal 
   or higher priority.
- The system will always prefer an agent that is not assigned any task to an agent 
   already assigned to a task.
- If all agents are currently working on a lower priority task, the system will pick the agent 
   that started working on his/her current task the most recently.

Logic:
    1) Find all agents that have the required skill set. If none, return empty set.
    2) Find all agents that are free. If so, pick one. 
    3) If no free agents, find agents that have priority less than new task.
    4) If more than one agents with low priority, find the one with newest task.

*/
function find_assignable_agent(agents, task) {

    var skilled_agents = find_skilled_agents(agents, task.skills);
    if (skilled_agents.length == 0) {
        return null;
    }
    var free_agents = find_free_agents(skilled_agents);
    if (free_agents.length > 0) {
        return free_agents[0]; //TODO: Can add logic to pick agent based on lowest ID etc. 
    } else {
        var lower_pty_agents = find_lower_pty_agents(skilled_agents, task.priority);
        if (lower_pty_agents.length > 0) {
            return find_most_recently_assigned_agent(lower_pty_agents);
        } else {
            return null;
        }
    }
};

/*
 * Find agent assigned to task and free it; return agent_id or null
 * 
 *  Logic: 
 *   1) Find agent the task is assigned to 
 *   2) Free that agent's task fields
 *   3) Return agent ID, or null if not found
 */
exports.complete = function(task) {
    var agent_id = find_agent_by_task_id(task.id);
    if (agent_id !== null) {
        if(agent_store.reset_agent(agent_id)) {
            return agent_id;
        } else {
            return null;
        }
    } else {
        return 0;
    }
}

/*
 * Assign agent to task if possible; return agent_id or null
 * 
 * Logic:
 *  1) Find agent the task can be assigned to
 *  2) If suitable agent found:
 *     a) Assign task to agent
 *     b) Return agent ID
 *  3) If no assignable agent found, return null.    
 */
exports.assign = function(task) {
     var agent = find_assignable_agent(agent_store.get_all_agents(), task);
    if (agent !== null) {
        agent.task_id = task.id;
        agent.task_pty = task.priority;
        agent.task_start = Date.now();
        if(agent_store.set_agent(agent)) {
            return agent.id;
        } else {
            return null;
        }
    } else {
        return null;
    }
}
