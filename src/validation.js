/*
 * Defines sample task and agent structures and helps validate tasks.
 *
 * (C) Copyright 2019 Rajat Ahuja <rajat@ahuja.name>.
 * 
 */

//Sample task:
var task = {
        id: '',       // Unique Task ID
        skills: [],   // List of required skills
        priority: '', // 'low' or 'high'
        agent_id: ''  // Agent assigned, or null
    };

//Sample agent:
var agent = {
    id: '',         // Unique Agent ID
    skillset: [],   // List of supported skills
    task_id: '',    // ID of task assigned, or null
    task_pty: '',   // Priority of task assigned, or null
    task_start: 0   // Task start time (ms since epoch), or null
};

exports.is_valid_task = function(task) {
    if (task && task.id && task.priority && task.skills && Array.isArray(task.skills)) {
        return true;
    } else {
        return false;
    }
}