/*
 * Loads, builds and stores in-memory agents.
 * 
 * TODO: Can possibly be replaced by a DB in future.
 *
 * (C) Copyright 2019 Rajat Ahuja <rajat@ahuja.name>.
 * 
 */

'use strict';

var fs = require('fs');

var agents = [];

// Load and build agents from a pre-defined list in a JSON-formatted file
exports.load_and_build_agents = function (agent_list_file, logger) {

    try {
        var agent_json = fs.readFileSync(agent_list_file);
    } catch {
        logger.error(`Could not load file ${agent_list_file}`);
        return 0;
    }

    try {
        agents = JSON.parse(agent_json);
    } catch {
        logger.error(`Could not parse agents from ${agent_list_file}`);
        return 0;
    }

    for (var agent of agents) {
        agent.task_id = null,
        agent.task_pty = null,
        agent.task_start = null
    }

    return agents.length;
};

// Returns all agents
exports.get_all_agents = function() {
    return agents;
};

// Resets agent by agent ID
exports.reset_agent = function(agent_id) {
    for (var agent of agents) {
        if (agent.id == agent_id) {
            agent.task_id = null;
            agent.task_pty = null;
            agent.task_start = null;
            return true;
        }
    }
    return false;
};

// Sets agent by ID with input agent
exports.set_agent = function(agent_in) {
    for (var agent of agents) {
        if (agent.id == agent_in.id) {
            agent.task_id = agent_in.task_id;
            agent.task_pty = agent_in.task_pty;
            agent.task_start = agent_in.task_start;
            return true;
        }
    }
    return false;
}